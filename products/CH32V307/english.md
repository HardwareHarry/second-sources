## Overview

The CH32V307 is a high-performance 32-bit microcontroller based on the RISC-V architecture. This microcontroller is tailored for a wide range of applications requiring high integration, low power consumption, and powerful processing capabilities.

The RISC-V MCU is tailor made with

- Qing Ke RISC-V V4F core, RV32IMACF
- Single cycle multiplication and division instructions.
- HPE hardware stack pushing
- VTF (Vector Table Free) 
- Two wire debugger
- WFE (Wait For Event) instruction for PFIC (Programmable Fast Interrupt Controller) 
- Custom compressed intructions for byte and nibble (4bit) operations.

## Key Features

- Running at a maximum frequency of 144 MHz.
- Up to 256KB of Flash memory and 64KB of SRAM.
- High-speed USB OTG interface
- Multiple UART, SPI, I2C, and ADC interfaces
- Timer with multiple functions (PWM, capture, etc.)
- RTC with calendar function
- Low Power Consumption: The CH32V307 supports various power-saving modes, making it ideal for battery-powered applications.
- Wide Operating Voltage Range: The device operates from 2.5V to 5.5V, ensuring compatibility with various power sources.

## Specifications
### General
- CPU: RISC-V 32-bit CPU Core
- Max Frequency: 144 MHz
- Temperature Range: -40°C to +85°C
- 2x 18 channel DMA
- CRC and 96bit UID

### Memory
- Flash: Up to 256KB
- SRAM: 64KB

### Interfaces
- USB: High-speed USB OTG/host/device interface (480MHz PHY)
- UART: 3xUSART+5xUART
- CAN: 2x 2.0BActive
- SDIO：1x
- FSMC: 1x
- DVP: 1x
- SPI: 3x
- I2C: 2x
- I2S: 2x
- ADC: 2x 16ch 12bit ADC, with 16x touch key
- OpAmp: 4x
- TRNG: 1x
- DAC: 2x 12bit
- ETH: 10M PHY
- Timers: 10x with various functions
- RTC: Real-time clock with calendar

### Power Consumption
- Low-power modes: Sleep, Stop, and Standby
- GPIO power supply: independent

### Voltage
- Operating Voltage: 2.5V to 5.5V
- Power up/Power down reset

## Applications
The CH32V307 is suitable for various applications, including but not limited to:

- Industrial automation
- Consumer electronics
- IoT devices
- Portable medical devices

## Development Support

WCH provides a comprehensive development ecosystem for the CH32V307, including development boards, software libraries, and development tools. This facilitates the design process and accelerates time-to-market for new products.

## Highlight

- Pin compatible to STM32F307
- Comprehensive peripherals for connectivies available in the package.
- A low cost EVB is available.
