GD32VF103 is a RISC-V microcontroller, which is pin to pin compatible to STM32F103. The chip is manufactured by Gigadevices in Shanghai. The key features:

- 108MHz CPU clock
- 16 to 128KB flash ROM
- 6 to 32KB SRAM
- 1x 16bit advanced timer
- 4x 16bit general purpose timers
- 2x 16bit basic timers
- 1x 24bit systick timer
- 2x watchdog timer
- 1x RTC timer
- 2x UART
- 3x USART
- 2x I2C
- 3x SPI
- 2x I2S audio interfaces
- 1x USB OTG
- 2x CAN 2.0B
- External memory interface for NOR/SRAM/PSRAM/ROM
- 2x12bit 16 channel ADC
- 2x12bit DAC
- High precision 1Msps ADC
- 2.6-3.6V, 5V tolerance GPIO, up to 80 I/Os
- Sleep, deep sleep, standby modes
- QFN36, LQFP48, LQFP64, LQFP100

Restrictions

- Mature RISC-V MCU
- FT2232 based JTAG interface has some restrictions on installation
- Segger JLINK V10 is working smoothly.