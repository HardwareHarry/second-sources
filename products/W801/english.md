W801 is a WiFi/BLE IoT SoC manufactured by Winner Micro, which is based in Beijing, China. It has rich on-chip peripherals. The key features are:

- 2.4GHz IEEE802.11b/g/n 
- Bluetooth/BLE 4.2 dual mode
- Integrated 32bit C-Sky V2 core
- General Purpose IO: GPIO/ADC
- Rich communication interfaces: UART/SPI/I2C
- Special interface: I2S/7816/SDIO
- UI interface: LCD/Touch sensor
- Memory interface: PSRAM
- Security module: TEE with hardware encryption algorithms
- Built-in DSP
- Built-in FPU
- Code protection
- 2MB XIP flash memory with code encryption, signature, secure debug, secure OTA
- GCC support
PS: C-Sky V1 was M-core from Motorola semiconductors, and C-Sky V2 was upgraded and revised.

Assessment
- XIP flash may have some restriction on execution speed and power consumptions.
- Some time critial code has to run in the RAM
- ADC precision is low and sampling rate is low.
- Lack of documentations and supports, although it is quite cheap.
- A complete SDK/toolchain is ready for OSHW community, but it is helpful for small modificaitons for application development.
- Recommended for DIYer for small projects.