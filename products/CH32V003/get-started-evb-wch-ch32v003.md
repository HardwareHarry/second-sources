# Get Started Manual

## Figure Out Hardware

In order to bring up the CH32V003 EVT/EVB, you have to:

1. Download documents and a zip file called EVT.zip of HAL drivers and demo projects.
2. Install MounRiver IDE, including GCC toolchain, device driver for WCH-LINK-E. 
3. Unzip EVT.zip to build selected demo projects in MounRiver.
4. Connect EVB to WCH-LINK-E over SDI, and connect WCH-LINK-E to your PC via USB.
5. Download the binary file with WCH-LINK-E to program and start to debug.
6. Disconnet EVB, apply a power cycle to it and run.

## Download Documents and Software

WCH offers data sheet, user manual and zipped EVT software, aka the software SDK. Yes, the documents
are written in English. 

## Install Toolchain and Drivers

WCH offers a complete IDE called "MounRiver" with C/C++ compilers, debugger and programmer.

## Unzip the EVT.zip File

## Build Demo Projects

You have two approaches to develop the sofware. One is to create a new project in mounriver with the 
project wizard and get it built, then you can download and go. The other is to unzip the EVT zip file
to arbitary folder, double click the project file and get it built and downloaded into the chip.

## Connect, Download and Debug

On the menu of MounRiver IDE, you can find the download button to program the chips.

## Disconnect and Run

## Resources

- https://pallavaggarwal.in/2023/09/20/ch32v003-programming-guide/
