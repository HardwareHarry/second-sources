HC32L110 is manufactured by HDSC (Huada Semiconductors). The key features are: 

- 32MHz Cortex-M0+ 32bit MCU
- Low power consumption
- 0.5uA @ 3V deep sleep mode
- 1.0uA @ 3V deep sleep + RTC mode
- 6uA @32.768kHz slow working mode
- 20uA/MHz @ 3V @ 16MHz sleep mode
- 120uA/MHz @ 3V @ 16MHz work mode
- 3uS ultra low power wakeup mode
- 16KB/32KB (embedded) flash memory, with write protection
- 2KB/4KB RAM with verification
- GPIO 16IO @ 20pin, 12IO @ 16pin
- Clock and Xtal
- External high crystal: 4M - 32MHz
- External low crystal: 32.768KHz
- Internal high clock: 4/8/16/22.12/24MHz
- Internal low clock: 32.768K/38.4KHz
- hardware clock calibration and supervisory circuit.
- Timer/Counter
- 3 x 16bit timers/counters
- 1 x low power 16bit timer/counter
- 3 x high performance 16bit timers/counters, with PWM complementation and dead-zone protection
- 1 x programmable 16bit timer/counter, with capture compare and PWM output
- 1 x 20bit programmable counter/WDT, with independent ultra low power RC-OSC.

Communications
- UART0/UART1 standard serial ports
- UART2 with low power clock
- SPI interface
- I2C interface
- Beep buzzer, with complementary outputs
- Hardware calendar RTC module
- Hardware CRC-16 module
- 16B Unique 16B chip ID
- 12bit 1Msps high speed high precision SARADC, with builtin op-amp to measure external weak signal
- Integrated 6bit DAC and programmable standard reference inputs 2 voltage comparator VC
- Lower Voltage Detector LVD, with 16 step comparsion voltages, to monitor port voltage and power supply
- Embedded debug support with SWD
- Working temperature: -40 ~ 85 
- Working voltage: 1.8-5.5V
- Package: QFN20, TSSOP20, TSSOP16

Assessment
- Ultra low cost, low power consumption, low pin count to replace STM32L0XX and LPC8XX.
- Limited ROM/RAM space
- Rich peripherals, with some unique features.
- Analog periperals have been tested in field yet.
- Popular as controller for SPI transceivers, toys and other veritical market.
- Supported by Keil uVision, IAR EW and GCC from OSHW communities.
- Recommended for new designs for mass productions.