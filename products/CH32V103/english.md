The CH32V103 is a generic microcontroller based upon RISC-V3A core. The key features are:

- Qing Ke (highland barley) RISC-V3A core, running up to 80MHz
- Single cycle hardware multiply/devide hardware
- 20KB SRAM with 64KB CodeFlash
- Power supply range: 2.7~5.5V
- Various power saving modes: sleep/stop/standy
- POR/PDR, power on reset and powder down reset
- PVD, programmable voltage detector
- 7 channle DMA controller
- 16x TouchKey channel monitoring
- 16x 12bit ADC 
- 7x timers
- 1x USB2.0 host/device interface (high/low speed)
- 2x I2C interface (PMBus/SMBus)
- 3x USART
- 2x SPI interfaces
- 51x GPIO, all can be mapped to 16 external interrupts
- CRC unit
- 96bit UID
- Serial two wire debug interface
- Available in LQFP64M/LQFP48/QFN48

