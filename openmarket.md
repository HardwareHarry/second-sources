## Open Market

The semiconductors can be divided into key-account driven or open market driven. Here I will list some products available from the open markets. I will remove mature enough product lines from the list, instead they will be out benchmarks. Such as STM32 in MCU, ESP32 in WiFi SoC, nRF52 in BLE SoC and etc.

### Industrial GP ARM MCU

- GD32F103
- AT32F103

### Industrial GP RISC-V MCU

- GD32VF103
- CH32V003
- CH32V203
- CH32V407

### Industrial GP MCU with Other Cores

- Winner Micro W806, CK802
- 8051/T1

### RFIC

- CTM1100 Sub-1GHz

### WiFi SoC

- Winner Micro W801, BLE/WiFi SoC
- ESP32-C3

### BLE SoC

- PHYPLUS PHY6212, BLE SoC
- PHYPLUS PHY6220/PHY6250, CK802 BLE SoC

### Combo RF SoC

- ESP32-C6

### High Performance MCU

- HPmicro HP6540 RISC-V 800MHz

### LPWAN

- Daolink, a MIMO based LPWAM solution

### MPU for Linux/Android SBC

- Rockchip PX30

### MPU for Embedded AI

- SG2002/CBV1800: Milk-V Duo Linux camera
- RV1103/RV1106: LuckFox Linux camera

### MEMS Sensors

- 6DoF
- Temperature
- Microphone

## IC for Makers

### TSSOP16/20

- PHY6222, TSSOP
- HC32L110, TSSOP
- PY32F003, TSSOP
- CKS/GD/GW/HK32F030, TSSOP
- CH32V003, TSSOP20

### QFN20

### QFP32

### QFP48

- STM32F103
- STM32F40X
- STM32F407

### QFP64

- AGM32RV100
- STM32F103
- STM32F40X
- STM32F407